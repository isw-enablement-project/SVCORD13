package com.isw.svcord13.api.v1;

import com.isw.svcord13.domain.svord.service.WithdrawalDomainService;
import com.isw.svcord13.sdk.api.v1.api.WithddrawalApiV1Delegate;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import com.isw.svcord13.sdk.api.v1.model.ErrorSchema;
import com.isw.svcord13.sdk.api.v1.model.WithdrawalBodySchema;
import com.isw.svcord13.sdk.api.v1.model.WithdrawalResponseSchema;
import com.isw.svcord13.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord13.sdk.domain.svord.entity.WithdrawalDomainServiceInput;
import com.isw.svcord13.sdk.domain.svord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord13.sdk.domain.svord.facade.SvordEntityBuilder;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * A stub that provides implementation for the WithddrawalApiV1Delegate
 */
@SuppressWarnings("unused")
@Service
@ComponentScan(basePackages = "com.isw.svcord13.sdk.api.v1.api")
public class WithddrawalApiV1Provider implements WithddrawalApiV1Delegate {

  @Autowired
  WithdrawalDomainService withdrawalDomainService;
  
  @Autowired
  DomainEntityBuilder entityBuilder;

  @Override
  public ResponseEntity<WithdrawalResponseSchema> servicingOrderWithdrawalPost(WithdrawalBodySchema withdrawalBodySchema) {
    //API호출 시 받은 데이터(Request body)를 서비스를 호출하기 위한 Input에 매칭 하고 호출
    //이 때 InputData는  entityBuilder를 통해 Setting 및 building을 해 준다. - build는 새로운 instnace생성
    SvordEntityBuilder svordEntityBuilder = entityBuilder.getSvord();
    WithdrawalDomainServiceInput withdrawalDomainServiceInput = svordEntityBuilder.getWithdrawalDomainServiceInput()
      .setAccountNumber(withdrawalBodySchema.getAccountNumber())
      .setAmount(withdrawalBodySchema.getAmount())
      .build();

    //서비스 호출 후 받은 결과를 API의 호출에 대한 Response를 생성하여 return한다.
    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = withdrawalDomainService.execute(withdrawalDomainServiceInput);

    if(withdrawalDomainServiceOutput == null){
      ErrorSchema errorSchema = new ErrorSchema();
      errorSchema.setErrorId("500");
      errorSchema.setErrorMsg("INTERNAL ERROR");
      ResponseEntity.status(500).body(errorSchema);
    }

    WithdrawalResponseSchema withdrawalResponse = new WithdrawalResponseSchema();

    withdrawalResponse.setServicingOrderWorkTaskResult(withdrawalDomainServiceOutput.getServicingOrderWorkResult().toString());
    withdrawalResponse.setTransactionID(withdrawalDomainServiceOutput.getTrasactionId());
    
    return ResponseEntity.status(200).body(withdrawalResponse);
  }

}
