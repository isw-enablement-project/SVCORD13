package com.isw.svcord13.integration.partylife.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord13.sdk.integration.partylife.service.LoginExcuteBase;
import com.isw.svcord13.sdk.integration.facade.IntegrationEntityBuilder;

@Service
public class LoginExcute extends LoginExcuteBase {

  private static final Logger log = LoggerFactory.getLogger(LoginExcute.class);

  public LoginExcute(IntegrationEntityBuilder entityBuilder ) { 
    super(entityBuilder );
  }
  
  @NewSpan
  @Override
  public com.isw.svcord13.sdk.integration.partylife.entity.LoginExcuteOutput execute(com.isw.svcord13.sdk.integration.partylife.entity.LoginExcuteInput loginExcuteInput)  {

    log.info("LoginExcute.execute()");
    // TODO: Add your service implementation logic
   
    return null;
  }

}
