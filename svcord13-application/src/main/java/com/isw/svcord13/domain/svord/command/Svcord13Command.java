package com.isw.svcord13.domain.svord.command;


import org.springframework.stereotype.Service;
import com.isw.svcord13.sdk.domain.svord.command.Svcord13CommandBase;
import com.isw.svcord13.sdk.domain.svord.entity.Svcord13;
import com.isw.svcord13.sdk.domain.svord.entity.Svcord13Entity;
import com.isw.svcord13.sdk.domain.svord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord13.sdk.domain.svord.type.ServicingOrderType;
import com.isw.svcord13.sdk.domain.svord.type.ServicingOrderWorkProduct;
import com.isw.svcord13.sdk.domain.svord.type.ServicingOrderWorkResult;
import com.isw.svcord13.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord13.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord13Command extends Svcord13CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord13Command.class);

  public Svcord13Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }

  
    
    @Override
    public com.isw.svcord13.sdk.domain.svord.entity.Svcord13 createServicingOrderProducer(com.isw.svcord13.sdk.domain.svord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
    

      log.info("Svcord13Command.createServicingOrderProducer()");
      Svcord13Entity servicingOrderProcedure = this.entityBuilder.getSvord().getSvcord13()
        .setCustomerReference(createServicingOrderProducerInput.getCustomerReference())
        .setProcessStartDate(LocalDate.now())
        .setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS)
        .setServicingOrderWorkDescription("CASH WITHDRAWALS")
        .setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT)
        .setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING)
        .build();
      
      ThirdPartyReferenceEntity thirdPartyReference = new ThirdPartyReferenceEntity();
      thirdPartyReference.setId("test1");
      thirdPartyReference.setPassword("password");
      servicingOrderProcedure.setThirdPartyReference(thirdPartyReference);

      return repo.getSvord().getSvcord13().save(servicingOrderProcedure);
    }
  
    
    @Override
    public void updateServicingOrderProducer(com.isw.svcord13.sdk.domain.svord.entity.Svcord13 instance, com.isw.svcord13.sdk.domain.svord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

      log.info("Svcord13Command.updateServicingOrderProducer()");
      
      Svcord13Entity servicingOrderProcedure = this.repo.getSvord().getSvcord13().getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
      servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessEndDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
      servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());

      log.info(updateServicingOrderProducerInput.getUpdateID().toString());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());

      this.repo.getSvord().getSvcord13().save(servicingOrderProcedure);
    }
  
}
