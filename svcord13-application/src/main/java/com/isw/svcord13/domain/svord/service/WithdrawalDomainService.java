package com.isw.svcord13.domain.svord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord13.sdk.domain.svord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord13.sdk.domain.svord.entity.CustomerReferenceEntity;
import com.isw.svcord13.sdk.domain.svord.entity.Svcord13;
import com.isw.svcord13.sdk.domain.svord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord13.sdk.domain.svord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord13.sdk.domain.svord.service.WithdrawalDomainServiceBase;
import com.isw.svcord13.sdk.domain.svord.type.ServicingOrderWorkResult;
import com.isw.svcord13.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord13.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord13.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord13.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord13.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord13.domain.svord.command.Svcord13Command;
import com.isw.svcord13.integration.partylife.service.RetrieveLogin;
import com.isw.svcord13.integration.paymord.service.PaymentOrder;
import com.isw.svcord13.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord13.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {
  @Autowired
  Svcord13Command svcord13Command;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder,  Repository repo) {
  
    super(entityBuilder,  repo);
    
  }
  
  @NewSpan
  @Override
  public com.isw.svcord13.sdk.domain.svord.entity.WithdrawalDomainServiceOutput execute(com.isw.svcord13.sdk.domain.svord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput)  {

    log.info("WithdrawalDomainService.execute()");

    //사용자 인증요청 Party Life Cycle Management 호출
    RetrieveLoginInput retrieveLoginInput = integrationEntityBuilder.getPartylife().getRetrieveLoginInput()
      //.setId(withdrawalDomainServiceInput.getId())
      .setId("test1")
      .build();
    RetrieveLoginOutput retrieveLoginOutput = retrieveLogin.execute(retrieveLoginInput);

    //User Valid 체크
    if(retrieveLoginOutput.getResult() != "SUCCESS") return null;

    CustomerReferenceEntity cusotmerReference = this.entityBuilder.getSvord().getCustomerReference()
      .setAccountNumber(withdrawalDomainServiceInput.getAccountNumber())
      .setAmount(withdrawalDomainServiceInput.getAmount())
      .build();

    //Root Entity 호출하여 새로운 레코드 생성
    CreateServicingOrderProducerInput createInput = this.entityBuilder.getSvord().getCreateServicingOrderProducerInput()
      .setCustomerReference(cusotmerReference)
      .build();
    
    Svcord13 createOutput = svcord13Command.createServicingOrderProducer(createInput);

    //현금 출금 요청을 위한  Payment Order 호출
    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput()
      .setAccountNumber(withdrawalDomainServiceInput.getAccountNumber())
      .setAmount(withdrawalDomainServiceInput.getAmount())
      .setExternalId("SVCORD13")
      .setExternalSerive("SVCORD13")
      .setPaymentType("CASH_WITHDRWAL")
      .build();
    PaymentOrderOutput paymentOrderOutput = paymentOrder.execute(paymentOrderInput);

    //Payment order결과를 DB에 업데이트 하고 해당 아웃풋을 전달
    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvord().getUpdateServicingOrderProducerInput()
      .setUpdateID(createOutput.getId().toString())
      .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderResult()))
      .build();
    
    svcord13Command.updateServicingOrderProducer(createOutput, updateServicingOrderProducerInput);

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvord().getWithdrawalDomainServiceOutput()
      .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderResult()))
      .setTrasactionId(paymentOrderOutput.getTransactionId())
      .build();
   
    return withdrawalDomainServiceOutput;
  }

}
